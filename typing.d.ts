declare var process: Process;
interface Process{
    env: Env
}

interface Env{
    WMM_FILE_EXPIRATION:string,
    BACKEND_PRIVATE_IP: string,
    BACKEND_URL: string
}

interface GlobalEnvironment{
    process:Process
}
