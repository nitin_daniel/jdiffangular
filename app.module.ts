import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { DialogContentComponent } from './dialog-content/dialog-content.component';
import { routes, routing } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material';
// import { MatFormFieldModule } from '@angular/material/form-field';

import { MatRadioModule } from '@angular/material/radio';
import { ServiceService } from './_service/service.service';
import { MatTableModule } from '@angular/material/table';
import { MainComponentResolver } from './main/main.component.resolver';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSortModule } from '@angular/material/sort';
import { LogoutComponent } from './logout/logout.component';
import { CountupTimerService } from './_service/countup-timer.service';
// import { AuthGuard } from './_service/auth.guard.service';
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { MsalModule } from "@azure/msal-angular";
import { MsalInterceptor } from "@azure/msal-angular";
import { LogLevel } from "msal";
import { HttpServiceHelper } from './common/HttpServiceHelper';
import { ErrorComponent } from './error.component'
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { DateFormatPipe } from './date-format.pipe';
import { I1 } from './interceptors';

export function loggerCallback(logLevel, message, piiEnabled) {
  console.log("client logging" + message);
}


export const protectedResourceMap: [string, string[]][] =
  [
    /*['https://13.66.59.218/dgt/datasetmanagement/'
      , ['api://02605cb2-b068-41f0-92fd-2b7345522f98/access_as_user']
    ],*/
    ['https://graph.microsoft.com/v1.0/me', ['user.read']]
  ];


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    DialogContentComponent,
    ConfirmationDialogComponent,
    LogoutComponent,
    ErrorComponent,
    DateFormatPipe
  ],
  imports: [
    routing,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    // MatFormFieldModule,
    MatRadioModule,
    MatTableModule,
    MatSortModule,
    NgbModule,
    MsalModule.forRoot({
      clientID: "02605cb2-b068-41f0-92fd-2b7345522f98",
      authority: "https://login.microsoftonline.com/6362b077-fb81-4d5b-adf3-129cdb1b56cf/",//tenant Id
      validateAuthority: true,
      redirectUri: "https://10.48.241.5:4200/",
      cacheLocation: "localStorage",
      navigateToLoginRequestUrl: true,
      popUp: false,
      consentScopes: ["user.read", "api://02605cb2-b068-41f0-92fd-2b7345522f98/access_as_user"],
      unprotectedResources: ["https://www.microsoft.com/en-us/"],
      protectedResourceMap: protectedResourceMap,
      logger: loggerCallback,
      correlationId: '1234',
      level: LogLevel.Info,
    })
  ],
  providers: [ServiceService, MainComponentResolver, ConfirmationDialogService, CountupTimerService,
    HttpServiceHelper,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: I1, multi: true },
    // { provide: HTTP_INTERCEPTORS, useClass: MsalInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent]
})
export class AppModule { }

// export function configLoader(appSettingsService: AppSettingsService) {
//   return () => appSettingsService.load();
// }
