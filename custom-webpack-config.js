
const webpack = require('webpack');
module.exports = {
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        WMM_FILE_EXPIRATION: JSON.stringify(process.env.WMM_FILE_EXPIRATION),
        BACKEND_PRIVATE_IP: JSON.stringify(process.env.BACKEND_PRIVATE_IP),
        BACKEND_URL: JSON.stringify(process.env.BACKEND_URL),
      }
    })
  ]
};
