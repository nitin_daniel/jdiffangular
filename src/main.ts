import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { ApplicationInsights } from '@microsoft/applicationinsights-web';

const appInsights = new ApplicationInsights({ config: {
  instrumentationKey: '5e7a86db-d760-4048-ac13-c69de3973f1b',
  enableAutoRouteTracking: true,
  maxBatchInterval: 10
} });
appInsights.loadAppInsights();

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule).then(() => {
}).catch(err => console.error(err));

