(function (window) {
  window.__env = window.__env || {};

  // API url
  //window.__env.apiUrl = 'https://10.48.241.4:8443'; // eec IP

  // window.__env.apiUrl = 'https://40.124.3.46:8080';
  window.__env.apiUrl = 'https://104.44.138.11:8080';

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;
}(this));
