import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  isCreateProcessRun: boolean = false;
  isExportProcessRun: boolean = false;
  isDeleteProcessRun: boolean = false;
  apiCall: boolean = false;
  deleteApiCall: boolean = false;
  getErrorResponse:any;
  getErrorResponseTxt: boolean = false;
}
