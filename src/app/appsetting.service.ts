// import { Inject } from '@angular/core';

// export class AppSettings{
//   backendurl:'';
//   baseApiUrl:'';
// }

// export class AppSettingsService extends BaseDataService {
//   appSettings: AppSettings;
//   constructor( @Inject(BASE_HTTP) private http) {
//       super();
//       this.appSettings = null;
//   }
//   setDebugSettings() {
//       if (environment.envName === 'uiTests') {
//           this.appSettings = new AppSettings({
//               // when building the solution in uiTest mode we specify different settings
//           baseApiUrl: '',
//           });
//       } else {
//           this.appSettings = new AppSettings({
//               // specify default debug settings
//           baseApiUrl: '',
//           });
//       }
//   }
//   load() {
//       return new Promise((resolve) => {
//           this.http.get('/assets/appSettings.php')
//               .map((res: any) => {
//                   // if we have a php tag then we are in the local where it's not served
//                   if (res.text().indexOf('<?php') > -1 || res.text().indexOf('<html>') > -1) {
//                       this.setDebugSettings();
//                   } else {
//                       let body;
//                       // check if empty, before calling json on the result
//                       if (res.text()) {
//                           body = res.json();
//                       }
//                       this.appSettings = new AppSettings(body, true);
//                       return body || {};
//                   }
//               })
//               .subscribe(config => {
//                    resolve();
//               });
//       });
//   }
// }
