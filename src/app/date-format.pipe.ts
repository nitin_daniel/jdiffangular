import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormat',
  pure: true
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any): any {
    var res = value.split(".");
    var res1 = res[0].replace('T',' ');
    return res1;
  }

}
