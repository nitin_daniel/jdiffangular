import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponentResolver } from './main/main.component.resolver';
import { MainComponent } from './main/main.component';
import { DialogContentComponent } from './dialog-content/dialog-content.component';
import { LogoutComponent } from './logout/logout.component';
import { MsalGuard } from "@azure/msal-angular";
import { ErrorComponent } from './error.component'

export const routes: Routes = [
  {
    path: 'main',
    component: MainComponent,
    // canActivate: [MsalGuard],
    resolve: { jdifResolve: MainComponentResolver },
  },
  {
    path: 'dialog',
    component: DialogContentComponent,
    // canActivate: [MsalGuard],
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: '', pathMatch: 'full', redirectTo: 'main'
  },
  { path: '**', component: ErrorComponent }


];
export const routing = RouterModule.forRoot(routes, { useHash: true });

