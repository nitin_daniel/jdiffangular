import { TestBed } from '@angular/core/testing';

import { CountupTimerService } from './countup-timer.service';

describe('CountupTimerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CountupTimerService = TestBed.get(CountupTimerService);
    expect(service).toBeTruthy();
  });
});
