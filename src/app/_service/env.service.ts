export class EnvService {

  // The values that are defined here are the default values that can
  // be overridden by env.js

  // API url
    public apiUrl = 'https://70.37.69.130:8080';
     // public apiUrl = 'https://10.48.241.4:8443'; //eec

  // Whether or not to enable debug mode
  public enableDebug = true;

  constructor() {
  }

}
