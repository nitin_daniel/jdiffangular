import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/observable/throw';
import { environment } from '../../environments/environment.prod';
import {EnvService} from '../_service/env.service';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient,private _envServ: EnvService,private _globals: Globals) {
    console.log("this._envServ.apiUrl::" + this._envServ.apiUrl)
  }

  private params = new HttpParams()
      .set('rejectUnauthorized', 'false')
      .set('requestCert', 'false')
      .set('insecure', 'true');

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
      'Access-Control-Allow-Credentials': 'true'
      ,'rejectUnauthorized': 'false'
    }),
    // params: this.params
  };
  databaseUrl = "/dgt-with-nola-db/datasetmanagement/";
  // "/dgt/datasetmanagement/"
  createUrl = 'https://70.37.69.130:8080' + this.databaseUrl + "create";
  refDataUrl ='https://70.37.69.130:8080' + this.databaseUrl + "getRefData";
  exportUrl = this._envServ.apiUrl + this.databaseUrl + "export";
  descUrl = 'https://70.37.69.130:8080' + this.databaseUrl + "getDescData";
  prgImpDataUrl = 'https://70.37.69.130:8080' + this.databaseUrl + "getProgressData?mode=IMPORT";
  prgExpDataUrl = this._envServ.apiUrl + this.databaseUrl + "getProgressData?mode=EXPORT";
  propertyReaderUrl = this._envServ.apiUrl + this.databaseUrl + "property";
  timerUrl = this._envServ.apiUrl + this.databaseUrl + "getTimer";
  connstatusUrl = this._envServ.apiUrl + this.databaseUrl + "getDBConnStatus";
  errorUrl = 'https://70.37.69.130:8080' + this.databaseUrl + "getErrors";

  // createUrl = this._envServ.apiUrl + this.databaseUrl + "create";
  // refDataUrl = this._envServ.apiUrl + this.databaseUrl + "getRefData";
  // exportUrl = this._envServ.apiUrl + this.databaseUrl + "export";
  // descUrl = this._envServ.apiUrl + this.databaseUrl + "getDescData";
  // prgImpDataUrl = this._envServ.apiUrl + this.databaseUrl + "getProgressData?mode=IMPORT";
  // prgExpDataUrl = this._envServ.apiUrl + this.databaseUrl + "getProgressData?mode=EXPORT";
  // propertyReaderUrl = this._envServ.apiUrl + this.databaseUrl + "property";
  // timerUrl = this._envServ.apiUrl + this.databaseUrl + "getTimer";
  // connstatusUrl = this._envServ.apiUrl + this.databaseUrl + "getDBConnStatus";
  // errorUrl = this._envServ.apiUrl + this.databaseUrl + "getErrors";

  getJdifData() {
       return this.http.get<any>('https://70.37.69.130:8080' + this.databaseUrl, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  getConnstaus() {
     return this.http.get<any>(this.connstatusUrl , this.httpOptions)
      .map(response => {
        return response;
      })
  }


  getBackendTimer() {
    let requestOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'text/plain; charset=utf-8',
        'Accept': 'text/plain; charset=utf-8',
        'rejectUnauthorized': 'false'
      }),
      responseType: 'text',
      // params: this.params
    }
    return this.http.get<any>(this.timerUrl, requestOptions)
      .map(response => {
        return response;
      })
  }

  getErrors(){
     return this.http.get<any>(this.errorUrl, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  getProgressImportData() {
    return this.http.get<any>(this.prgImpDataUrl, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  getProgressExportData() {
    return this.http.get<any>(this.prgExpDataUrl, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  getTxtData() {
     return this.http.get<any>(this.descUrl, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  getRefData() {
     return this.http.get<any>(this.refDataUrl, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  getPropertyFileData(property: any) {
    let params = new HttpParams()
      .set('key', property)
      .set('rejectUnauthorized', 'false')
      .set('requestCert', 'false')
      .set('insecure', 'true');
    let requestOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'text/plain; charset=utf-8',
        'Accept': 'text/plain; charset=utf-8',
        'rejectUnauthorized': 'false'
      }),
      responseType: 'text',
      params: params
    }
    return this.http.get<any>(this.propertyReaderUrl, requestOptions)
      .map(response => {
        return response;
      })
  }

  createJdifData(payload: any) {
    return this.http.post<any>('https://10.48.241.4:8443/dgt/datasetmanagement/create', payload, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  exportJdifData(payload: any) {
    this._globals.apiCall = true;
     return this.http.post<any>(this.exportUrl, payload, this.httpOptions)
      .map(response => {
        return response;
      })
  }

  deleteJdifData(dataset: any) {
    this._globals.deleteApiCall = true;
     return this.http.delete<any>( this._envServ.apiUrl + this.databaseUrl  + dataset, this.httpOptions)
      .map(response => {
        return response;
      })
  }
}
