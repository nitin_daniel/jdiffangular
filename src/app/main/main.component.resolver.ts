import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { ServiceService } from '../_service/service.service';
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { forkJoin } from 'rxjs';
import {Globals} from '../globals';

@Injectable()
export class MainComponentResolver implements Resolve<any>{

  constructor(private _router: Router, private _service: ServiceService,private _globals : Globals) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    let respJdifData = this._service.getJdifData();
    let respError = this._service.getErrors();
    return forkJoin([respJdifData,respError])
    // return forkJoin([respJdifData])
      .catch(() => {
        //return Observable.of('jdif dataset not available at this time');
        return forkJoin([]);
      });
  }
}
