import { Component, OnInit, ViewChild, Input, OnDestroy, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DialogContentComponent } from '../dialog-content/dialog-content.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog'
import { FormGroup } from '@angular/forms';
import { ServiceService } from '../_service/service.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { Globals } from '../globals';
import { CountupTimerService } from '../_service/countup-timer.service';
import { countUpTimerConfigModel, timerTexts } from '../_service/countup-timer.model';
import { BroadcastService } from "@azure/msal-angular";
import { MsalService } from "@azure/msal-angular";
import { Subscription } from "rxjs/Subscription";
import { HttpServiceHelper } from "../common/HttpServiceHelper";
import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { TimeoutError } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy, AfterViewInit {
  // , { static: false }
  @ViewChild(MatTable) table: MatTable<any>;
  @Input() startTime: String;
  @Input() countUpTimerConfig: countUpTimerConfigModel;

  //Init
  timerObj: any = {};
  private timerSubscription;
  timerConfig: countUpTimerConfigModel;
  timerTextConfig: timerTexts;

  form: FormGroup;
  displayedColumns: string[] = ['Name', 'Description', 'Status', 'Import Timestamp'];
  selection = new SelectionModel<any>(false, []);
  dataSource: any;
  txtAreaVal: any;
  prgImpData: any;
  prgExpData: any;
  isEnableExport: boolean = false;
  isEnableDelete: boolean = false;
  isEnableButton: boolean = true;
  jdifResolveResp: any;
  onClickRes: any = {};
  ELEMENT_DATA: any[] = [];
  dataset: string = '';
  status: any;
  datbaseStatus: any;
  fileExpiration: any;
  rowdisable: boolean = false;
  accessToken: string;
  timerTxt: string;
  userName: any;
  deleteErr: boolean = false;
  exportErr: boolean = false;
  databaseErr: boolean = false;
  databaseErrTxt: string;
  @ViewChild(MatPaginator) paginator: MatPaginator; //, { static: false }
  @ViewChild(MatSort) sort: MatSort; //, { static: false }
  respArr = [];
  rowArr = [];
  backendstartTimer: any;
  timerex: boolean = false;
  timerData: any;
  timerexVar: any;
  isCreateRun: boolean = false;
  isExportRun: boolean = false;
  isDeleteRun: boolean = false;
  isCreateProcessRun: boolean = false;
  isExportProcessRun: boolean = false;
  isDeleteProcessRun: boolean = false;
  private subscription: Subscription;
  checkImpOrExp: string = 'import';
  timerCall: boolean = true;
  exportErrText: any;
  deleteErrText: any;
  createErrText: any;
  createErr: boolean = false;
  exportRes: any = "";
  txtAreaComp: boolean = false;
  userData;
  clickedIndex;
  rowClicked;
  backgroundProcessrunning: boolean = false;
  errorResponse: any;
  url = "https://graph.microsoft.com/v1.0/me";

  constructor(private _service: ServiceService, private _router: ActivatedRoute, private router: Router,
    public dialog: MatDialog, private _confirmationDialogService: ConfirmationDialogService, private httpService: HttpServiceHelper,
    private countupTimerService: CountupTimerService, private globals: Globals,
    //  private authService: MsalService, private broadcastService: BroadcastService
  ) {
    this.respArr = this._router.snapshot.data['jdifResolve'][0];
    this.errorResponse = this._router.snapshot.data['jdifResolve'][1];
    if ((this.respArr).length == 0) {
      this.databaseErr = true;
      this.databaseErrTxt = "Unable to fetch the data from database........";
      this.jdifResolveResp = [];
    } else {
      // Array and not empty
      this.jdifResolveResp = this._router.snapshot.data['jdifResolve'][0];
    }
    //this.jdifResolveResp = this._router.snapshot.data['jdifResolve'][0];
    this.ELEMENT_DATA = (this.jdifResolveResp).datasetManagementList;
    this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
    this._service.getPropertyFileData("wmm.file.expiration").subscribe((data) => {
      this.fileExpiration = data;
    },
      (err) => {
        console.log("err in property file::" + err);
      })
    // this.fileExpiration = environment.WMM_FILE_EXPIRATION;
    var consVar = setInterval(() => {
      this.textAreaFunct(consVar);
      this.rowdisable = true;
      if (!this.txtAreaComp) {
        if ((this.checkImpOrExp).indexOf('import') !== -1) {
          this.progressBarImportMainFunc(consVar, "importMtd");
        } else if ((this.checkImpOrExp).indexOf('export') !== -1) {
          this.progressBarExportMainFunc(consVar, "exportMtd");
        }
      }
    }, 2000);
  }

  getErrorsApiCall() {
    this._service.getErrors().subscribe((data) => {
      this.errorResponse = data.description;
    },
      (err) => {
        console.log("error in getErrors methdod::" + err);
      })
  }

  getTimerRestAPICall() {
    this._service.getBackendTimer().subscribe(
      (data) => {
        if (data !== null || data !== "") {
          let result = (data.toString()).split(":");
          let originalData = result[1];
          if (originalData !== null || originalData !== "") {
            this.timerData = originalData;
            this.timerex = true;
            let str = this.timerData.split(" ");
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            let h = "", mm, ss, hours;
            for (let i = 0; i < str.length; i++) {
              if (str.length == 8) {
                if (str[i] === "h,") {
                  h = str[i - 1];
                }
              }
              if (str.length == 7) {
                let splitArr = str[2].split("h");
                h = splitArr[0];
              }
              if (str.length == 6) {
                h = "0";
              }
              if (str[i] === "min,") {
                mm = str[i - 1];
                if (mm.length == 1) {
                  let zero = "0";
                  mm = "0".concat(mm);
                } else if (mm > 60) {
                  mm = 60;
                }
              }
              if (str[i] === "sec") {
                ss = str[i - 1];
                if (ss > 60) {
                  ss = 60;
                }
              }
            }
            h = "0" + h;
            console.log(h + ":" + mm + ":" + ss);
            let dt = new Date();
            dt.setHours(dt.getHours() - parseInt(h));
            dt.setMinutes(dt.getMinutes() - parseInt(mm));
            dt.setSeconds(dt.getSeconds() - parseInt(ss));
            this.backendstartTimer =
              months[dt.getMonth()] +
              " " +
              dt.getDate() +
              "," +
              dt.getFullYear() +
              " " +
              dt.getHours() +
              ":" +
              dt.getMinutes() +
              ":" +
              dt.getSeconds();
            console.log("backendstartTimer:" + this.backendstartTimer);
            this.startTimer();
          }
        }

      },
      (err) => {
        console.log("err in getTimer:: " + err + "\n" + JSON.stringify(err));
      }
    );
  }

  timerInitialize() {
    this.getTimerValue();
    this.timerConfig = new countUpTimerConfigModel();
    this.timerTextConfig = new timerTexts();
    this.timerConfig = this.countUpTimerConfig ? Object.assign(this.countUpTimerConfig) : null;
    this.timerTextConfig = this.countUpTimerConfig && this.countUpTimerConfig.timerTexts ? Object.assign(this.countUpTimerConfig.timerTexts) : null;
  }

  ngOnInit() {

    this.timerInitialize();

    //  this.getUSerProfile();
    //  let user = this.authService.getUser();
    //  this.userName = user.name;
    //  this.subscription = this.broadcastService.subscribe("msal:acquireTokenSuccess", (payload) => {
    //    console.log("acquire token success " + JSON.stringify(payload));
    //  });

    //will work for acquireTokenSilent and acquireTokenPopup
    //  this.subscription = this.broadcastService.subscribe("msal:acquireTokenFailure", (payload) => {
    //    console.log("acquire token failure " + JSON.stringify(payload))
    //    if (payload.indexOf("consent_required") !== -1 || payload.indexOf("interaction_required") != -1) {
    //      this.authService.acquireTokenPopup(["user.read", "mail.send"]).then((token) => {
    //        console.log("in user token::" + token + "\n" + JSON.stringify(token));
    //        this.getUSerProfile();
    //      }, (error) => {
    //      });
    //    }
    //  });

    this._service.getConnstaus().subscribe(
      (data) => {
        let statusStr = (data.connection).split(" ");
        this.status = statusStr[1];
        this.datbaseStatus = statusStr[2];
      },
      (error) => {
        console.log("error getConnstaus :: " + JSON.stringify(error));
      }
    );

  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

   getUSerProfile() {
     this.httpService.httpGetRequest(this.url)
       .subscribe(data => {
         this.userData = data;
       }, error => {
         console.error(" Http get request to MS Graph failed" + JSON.stringify(error));
       });
   }

  textAreaScroll() {
    let ele = document.getElementById('exampleFormControlTextarea3') as HTMLTextAreaElement;
    ele.scrollTop = ele.scrollHeight;
  }

  onRowClicked(row, idx) {
    this.onClickRes = row;
    this.exportErr = false;
    this.deleteErr = false;
    if (this.globals.isCreateProcessRun || this.isExportProcessRun || this.isDeleteProcessRun) {
      //console.log("if st::" + this.globals.isCreateProcessRun + this.isExportProcessRun + this.isDeleteProcessRun)
      /*disable all three buttons */
      this.isEnableExport = false;
      this.isEnableDelete = false;
      this.isEnableButton = true;
    } else if (this.isCreateRun || this.isExportRun || this.isDeleteRun) {
      //console.log("else if::" + this.isCreateRun + this.isExportRun + this.isDeleteRun)
      /*disable all three buttons */
      this.isEnableExport = false;
      this.isEnableDelete = false;
      this.isEnableButton = true;
    } else {
      if (this.rowClicked === idx) this.rowClicked = -1;
      else this.rowClicked = idx;
      if (this.rowClicked != -1) {
        //console.log("292::" + this.rowClicked)
        this.isEnableExport = true;
        this.isEnableDelete = true;
        this.isEnableButton = true;
      } else {
        //console.log("296::" + this.rowClicked);
        this.isEnableExport = false;
        this.isEnableDelete = false;
        this.isEnableButton = false;
      }
    }
  }


  /*
   export function called when user clicks on Export button
  */
  exportfunc() {
    let exportTestRun: boolean = false;
    this.createErrText = '';
    this.exportErrText = '';
    this.deleteErrText = '';
    // console.log("clickedIndex::" + this.clickedIndex + "\n" + JSON.stringify(this.dataSource.data[this.clickedIndex - 1]));
    /* on export click disable create,export, delete buttons */
    this.isEnableExport = false;
    this.isEnableDelete = false;
    this.isEnableButton = true;
    var postReq = {
      "datasetId": (this.onClickRes).datasetId,
      "tablespaceName": (this.onClickRes).tablespaceName,
      "importTimestamp": (this.onClickRes).importTimestamp,
      "isUsable": (this.onClickRes).isUsable,
      "description": (this.onClickRes).description
    }
    this.isExportProcessRun = true;
    this.timerTxt = "Exporting ";
    // localStorage.setItem('isExportProcessRun', 'true');
    this.rowdisable = true;
    this._service.exportJdifData(JSON.stringify(postReq)).subscribe(
      (data) => {
        this.table.renderRows();
        if ((data.message).indexOf("Dataset exported successfully") != -1) {
          this.exportRes = "Dataset exported successfully";
          this.pauseTimer();
          this.selection.clear();
          this.rowdisable = false;
          this.isExportProcessRun = false;
          // localStorage.setItem('isExportProcessRun', 'false');
          // localStorage.removeItem("apiCall");
          this.globals.apiCall = false;
          exportTestRun = true;
          /* on export success enable create and disable export, delete buttons */
          this.isEnableButton = false;
          this.isEnableExport = false;
          this.isEnableDelete = false;
        }
      }, (error) => {
        console.log("err in export jdif data :: " + error);
        this.pauseTimer();
        this.stopTimer();
        this.selection.clear();
        this.rowdisable = false;
        this.exportErr = true;
        this.exportErrText = "Error while exporting the dataset";
        this.isExportProcessRun = false;
        // localStorage.setItem('isExportProcessRun', 'false');
        // localStorage.removeItem("apiCall");
        this.globals.apiCall = false;
        /* on export success enable create and disable export, delete buttons */
        this.isEnableButton = false;
        this.isEnableExport = false;
        this.isEnableDelete = false;
        // this.selection.deselect(this.dataSource.data[this.clickedIndex - 1]);
      });
    // localStorage.getItem("apiCall") === "true" || localStorage.getItem('isExportProcessRun') === 'true' ||
    if (this.globals.apiCall) {
      this.startTimer();
      var myExpVar = setInterval(() => {
        if (!this.exportErr && this.isExportProcessRun) {
          this.textAreaFunct(myExpVar, "export");
          this.progressBarExportMainFunc(myExpVar, "exportMtd");
        }if(exportTestRun){
          this.textAreaFunct(myExpVar, "export");
          this.progressBarExportMainFunc(myExpVar, "exportMtd");
          exportTestRun = false;
        }
      }, 2000);
    }

  }

  /*
   delete function called when user clicks on delete button
  */
  deletefunc() {
    let deleteTestRun: boolean = false;
    this.createErrText = '';
    this.exportErrText = '';
    this.deleteErrText = '';
    /* on delete click disable create,export, delete buttons */
    this.isEnableExport = false;
    this.isEnableDelete = false;
    this.isEnableButton = true;
    this._confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete... ?')
      .then((confirmed) => {
        if (confirmed) {
          this.isDeleteProcessRun = true;
          // localStorage.setItem('isDeleteProcessRun', 'true');
          this.timerTxt = "Deleting ";
          this.rowdisable = true;
          this._service.deleteJdifData((this.onClickRes).tablespaceName).subscribe(
            (data) => {
              if ((data.message).indexOf('Dataset deleted successfully.') != -1) {
                this.pauseTimer();
                this.selection.clear();
                this.rowdisable = false;
                this.isDeleteProcessRun = false;
                // localStorage.setItem('isDeleteProcessRun', 'false');
                this.globals.deleteApiCall = false;
                deleteTestRun = true;
                // localStorage.removeItem("deleteApiCall");
                // update table data
                this.jdifDataTableFunc();
                /* on delete success enable create and disable export, delete buttons */
                this.isEnableButton = false;
                this.isEnableExport = false;
                this.isEnableDelete = false;
              }
              this.table.renderRows();
            }, (error) => {
              console.log("error::" + error);
              this.pauseTimer();
              this.stopTimer();
              this.selection.clear();
              this.rowdisable = false;
              // localStorage.removeItem("deleteApiCall");
              this.globals.deleteApiCall = false;
              this.deleteErr = true;
              this.deleteErrText = "Error while deleting the dataset...";
              this.isDeleteProcessRun = false;
              // localStorage.setItem('isDeleteProcessRun', 'false');
              /* on delete success enable create and disable export, delete buttons */
              this.isEnableButton = false;
              this.isEnableExport = false;
              this.isEnableDelete = false;

            });

          if (this.globals.deleteApiCall) {
            this.startTimer();
            var mydelVar = setInterval(() => {
              if (!this.deleteErr && this.isDeleteProcessRun) {
                // call textarea function
                this.textAreaFunct(mydelVar, "deleteMtd");
              }
              if(deleteTestRun){
                this.textAreaFunct(mydelVar, "deleteMtd");
                deleteTestRun = false;
              }
            }, 2000);
          }
        } else {
          /*disable create and enable export and delete */
          this.isEnableButton = true;
          this.isEnableExport = true;
          this.isEnableDelete = true;
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  openDialog() {
    this.createErrText = '';
    this.exportErrText = '';
    this.deleteErrText = '';
    const dialogRef = this.dialog.open(DialogContentComponent, {
      width: '750px',
      data: {
        value: false,
        form: this.form
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(
      result => {
        // console.log('The dialog was closed', result.value + "\n" + result.form);
        // this.progressBarFunc(parseInt("0"));
        this.table.renderRows();
        if (result.value) {
          /* disable create , export, delete buttons */
          this.isEnableButton = true;
          this.isEnableExport = false;
          this.isEnableDelete = false;
          this.timerTxt = " Importing ";
          this.startTimer();
          this.rowdisable = true;
          var myVar = setInterval(() => { //localStorage.getItem('isCreateProcessRun') === 'true' ||
            if (this.globals.isCreateProcessRun) {
              this._service.getTxtData().subscribe(
                (data) => {
                  if ((data.description).indexOf("Completed export.") !== -1) {
                    this.pauseTimer();
                    this.txtAreaVal = data.description;
                    this.textAreaScroll();
                    this.rowdisable = false;
                    this.globals.isCreateProcessRun = false;
                    // localStorage.setItem('isCreateProcessRun', 'false');
                    clearInterval(myVar);
                    // update table data
                    this.jdifDataTableFunc();
                    /* enable create and disable export, delete buttons */
                    this.isEnableButton = false;
                    this.isEnableExport = false;
                    this.isEnableDelete = false;

                  } else {
                    this.txtAreaVal = data.description;
                    this.textAreaScroll();
                  }
                  this.table.renderRows();
                }, (error) => {
                  console.log("error in dialog reference:" + error);
                  this.rowdisable = false;
                  this.globals.isCreateProcessRun = false;
                  this.stopTimer();
                  // localStorage.setItem('isCreateProcessRun', 'false');
                  this.createErr = true;
                  this.createErrText = "Error while creating the dataset";
                  /* enable create and disable export, delete buttons */
                  this.isEnableButton = false;
                  this.isEnableExport = false;
                  this.isEnableDelete = false;

                  clearInterval(myVar);
                });
              this.progressBarImportMainFunc(myVar, "importMtd")
            }
          }, 2000);
        }
      });
  }

  textAreaFunct(myVar?, funcName?) {
    if ((this.errorResponse.description).indexOf("No Errors") == -1) // got some error
    {
      this.isCreateRun = false;
      this.isExportRun = false;
      this.isDeleteRun = false;
      /* on textarea success enable create and disable export, delete buttons */
      this.isEnableButton = false;
      this.isEnableExport = false;
      this.isEnableDelete = false;

      this._service.getTxtData().subscribe(
        (data) => {
          this.txtAreaVal = data.description;
          this.textAreaScroll();
        });
      if (myVar != undefined) { clearInterval(myVar); }
    } else {
      this._service.getTxtData().subscribe(
        (data) => {
          if ((data.description).indexOf("Completed export.") !== -1 || (data.description) === "") {
            this.txtAreaComp = true;
            if (this.txtAreaComp) { this.pauseTimer(); }
            this.rowdisable = false;
            this.txtAreaVal = data.description;
            this.isCreateRun = false;
            this.isExportRun = false;
            /* on textarea success enable create and disable export, delete buttons */
            this.isEnableButton = false;
            this.isEnableExport = false;
            this.isEnableDelete = false;
            this.textAreaScroll();
            if (myVar != undefined) { clearInterval(myVar); }
            if (this.timerexVar != undefined) { clearInterval(this.timerexVar); }
          } else if (((data.description).indexOf("Deleting dataset DatasetManagement") !== -1 && (data.description).indexOf("complete") !== -1) || (data.description) === "") {
            this.txtAreaComp = true;
            if (this.txtAreaComp) { this.pauseTimer(); }
            this.txtAreaVal = data.description;
            this.isDeleteRun = false;
            /* on textarea success enable create and disable export, delete buttons */
            this.isEnableButton = false;
            this.isEnableExport = false;
            this.isEnableDelete = false;
            this.textAreaScroll();
            if (myVar != undefined) { clearInterval(myVar); }
            if (this.timerexVar != undefined) { clearInterval(this.timerexVar); }
          } else if ((data.description).indexOf("failed to delete DatasetManagement") !== -1) {
            this.txtAreaComp = true;
            this.txtAreaVal = data.description;
            this.isDeleteRun = false;
            /* on textarea fail delete enable create and disable export, delete buttons */
            this.isEnableButton = false;
            this.isEnableExport = false;
            this.isEnableDelete = false;
            this.textAreaScroll();
            if (myVar != undefined) { clearInterval(myVar); }
          } else {
            this.txtAreaComp = false;
            if ((data.description).indexOf("Creating dataset DatasetManagement") !== -1) {
              this.isCreateRun = true;
              this.checkImpOrExp = 'import';
              this.timerTxt = "Importing ";
              this.txtAreaVal = '';
            } else if ((data.description).indexOf("Exporting") !== -1) {
              this.isExportRun = true;
              this.checkImpOrExp = 'export';
              this.timerTxt = "Exporting ";
              this.txtAreaVal = '';
            } else if ((data.description).indexOf("Deleting dataset DatasetManagement") !== -1) {
              this.isDeleteRun = true;
              this.timerTxt = "Deleting ";
            }
            if (!this.txtAreaComp) {
              if (this.timerCall) {
                this.getTimerRestAPICall();
                this.timerCall = false;
              }
            }
            this.txtAreaVal = data.description;
            this.textAreaScroll();
            this.timerex = false;
          }
          this.table.renderRows();
        }, (err) => {
          this.stopTimer();
          this.rowdisable = false;
          this.isDeleteRun = false;
          this.isCreateRun = false;
          this.isExportRun = false;
          /*desc data error */

          this.selection.clear();
          /*enable create and disable export and delete */
          this.isEnableButton = false;
          this.isEnableExport = false;
          this.isEnableDelete = false;
          if (myVar != undefined) { clearInterval(myVar); }
          if (err instanceof TimeoutError) {
            console.log("error ==> timed out in text area funct")
          } else if (err instanceof HttpErrorResponse) {
            console.log("error ==> http error in text area funct")
          } else {
            console.log("error in textarea func::" + err + JSON.stringify(err));
          }
          if (myVar != undefined) { clearInterval(myVar); }
        });
    }

  }

  progressBarImportMainFunc(myInterval?, funcName?) {
    if (funcName.indexOf("importMtd") !== -1) {
      this._service.getProgressImportData().subscribe(
        (data) => {
          this.prgExpData = parseInt(data.progressdata);
          if(this.prgExpData <= 99){
            this.progressBarFunc(parseInt(this.prgExpData));
          }else if(this.prgExpData >= 100){
            this.progressBarFunc(parseInt("100"));
          }
          if (this.prgExpData == 100) {
            if (myInterval != undefined) { clearInterval(myInterval); }
          }
          this.table.renderRows();
        }, (error) => {
          console.log("progress bar import func::" + error + "\n" + JSON.stringify(error));
          if (myInterval != undefined) { clearInterval(myInterval); }
        });

    }
  }

  progressBarExportMainFunc(myIntVar?, funcName?) {
    if (funcName.indexOf("exportMtd") !== -1) {
      this._service.getProgressExportData().subscribe(
        (data) => {
          this.prgExpData = parseInt(data.progressdata);
          this.progressBarFunc(parseInt(this.prgExpData));
          if (this.prgExpData == 100) {
            if (myIntVar != undefined) { clearInterval(myIntVar); }
          }
          this.table.renderRows();
        }, (error) => {
          console.log("progress bar export func::" + error + "\n" + JSON.stringify(error));
          if (myIntVar != undefined) { clearInterval(myIntVar); }
        });

    }
  }

  jdifDataTableFunc() {
    this._service.getJdifData().subscribe(data => {
      this.ELEMENT_DATA = data.datasetManagementList;
      this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
    }, (error) => {
      console.log("error in jdifDataTable Func::" + error);
    });
  }

  progressBarFunc(current_progress, methodname?) {
    $("#dynamic")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "% Complete");
  }

  //get timer value
  getTimerValue = () => {
    this.timerSubscription = this.countupTimerService.getTimerValue().subscribe(res => {
      this.timerObj = Object.assign(res);
    }, error => {
      console.log(error);
      console.log('Failed to get timer value');
    });
  }

  startTimer = () => {
    this.stopTimer();
    if (this.timerex) {
      this.countupTimerService.startTimer(this.backendstartTimer);
      this.timerInitialize();
    } else {
      this.countupTimerService.startTimer();
    }
  }

  stopTimer = () => {
    this.countupTimerService.stopTimer();
  }

  pauseTimer = () => {
    this.countupTimerService.pauseTimer();
  }

  // logout() {
  //    this.authService.logout();
  // }

  ngOnDestroy() {
    this.stopTimer();
    this.timerSubscription.unsubscribe();

    //  this.broadcastService.getMSALSubject().next(1);
    //  if (this.subscription) {
    //    this.subscription.unsubscribe();
    //  }

  }

}
