import { Component, OnInit } from '@angular/core';
import { BroadcastService } from "@azure/msal-angular";
import { MsalService } from "@azure/msal-angular";
import { Subscription } from "rxjs/Subscription";
import { Event, Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'JdifAngular';
  loggedIn: boolean;
  public userInfo: any = null;
  showLoadingIndicator = true;
  private subscription: Subscription;
  public isIframe: boolean;

  constructor(
    // private broadcastService: BroadcastService,
    // private authService: MsalService,
    private _router: Router) {

    this._router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator = true;
      }
      if (routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationCancel ||
        routerEvent instanceof NavigationError) {
        this.showLoadingIndicator = false;
      }
    });

    //This is to avoid reload during acquireTokenSilent() because of hidden iframe
    //   this.isIframe = window !== window.parent && !window.opener;
    //   if (this.authService.getUser()) {
    //     this.loggedIn = true;
    //   }
    //   else {
    //     this.loggedIn = false;
    //     this.login();
    //   }
    }

    // login() {
    //   this.authService.loginPopup(["user.read"]);
    // }

    // logout() {
    //   this.authService.logout();
    // }
    ngOnInit() {

      // this.broadcastService.subscribe("msal:loginFailure", (payload) => {
      //   console.log("login failure " + JSON.stringify(payload));
      //   this.loggedIn = false;

      // });

      // this.broadcastService.subscribe("msal:loginSuccess", (payload) => {
      //   console.log("login success " + JSON.stringify(payload));
      //   this.loggedIn = true;
      // });
    }
  }
